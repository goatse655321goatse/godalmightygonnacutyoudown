<?php
/**
 * Реализовать возможность входа с паролем и логином с использованием
 * сессии для изменения отправленных данных в предыдущей задаче,
 * пароль и логин генерируются автоматически при первоначальной отправке формы.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');
$abilities = ['Immortality' => 'Бессмертие', 'Througn_the_walls'=> 'Леха', 'Levitation' => 'Левитация'];
// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    setcookie('login', '', 100000);
    setcookie('passw', '', 100000);
    // Выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
    // Если в куках есть пароль, то выводим сообщение.
    if (!empty($_COOKIE['passw'])) {
      $messages[] = sprintf('<br>Вы можете <a href="login.php">войти<br></a> с логином <strong>%s</strong>
        и паролем<br> <strong>%s</strong> для изменения данных.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['passw']));
    }
  }

  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['date'] = !empty($_COOKIE['date_error']);
  $errors['limbs'] = !empty($_COOKIE['limbs_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['abilities'] = !empty($_COOKIE['abilities_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);
  $errors['agree'] = !empty($_COOKIE['agree_error']);

  // TODO: аналогично все поля.

  // Выдаем сообщения об ошибках.
  if ($errors['fio']) {
    setcookie('fio_error', '', 100000);
    if($_COOKIE['fio_error'] == '1') {
      $messages[] = '<div class="error">Заполните имя.</div>';
    } else {
      $messages[] = '<div class="error">Проверьте символы.</div>';
    }
  }

  if ($errors['email']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('email_error', '', 100000);
    // Выводим сообщение.
    if($_COOKIE['email_error'] == '1') {
      $messages[] = '<div class="error">Заполните адрес электронной почты.</div>';
    } else {
      $messages[] = '<div class="error">Проверьте символы.</div>';
    }
  }

  if ($errors['date']) {
    //Удаляем куку, указывая время устаревания в прошлом.
    setcookie('date_error', '', 100000);
    //Выводим сообщение.
    $messages[] = '<div class="error">Выберите год.</div>';
  }

  if ($errors['sex']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('sex_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Выберите пол.</div>';
  }

  if ($errors['abilities']) {
    setcookie('abilities_error', '', 100000);
    $messages[] = '<div class="error">Выберите способность.</div>';
  }

  if ($errors['limbs']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('limbs_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Выберите количество конечностей.</div>';
  }

  if ($errors['biography']) {
      setcookie('biography_error', '', 100000);
      $messages[] = '<div class="error">Расскажите о себе.</div>';
  }

   if ($errors['agree']) {
      setcookie('agree_error', '', 100000);
      $messages[] = '<div class="error">Согласитесь, иначе форма не отправится.</div>';
  }
  // TODO: тут выдать сообщения об ошибках в других полях.

  // Складываем предыдущие значения полей в массив, если есть.
  // При этом санитизуем все данные для безопасного отображения в браузере.
  $values = array();
  $values['fio'] = (empty($_COOKIE['fio_value']) || !preg_match('/^[а-яА-Я ]+$/u', $_COOKIE['fio_value'])) ? '' : strip_tags($_COOKIE['fio_value']);
  $values['email'] = (empty($_COOKIE['email_value']) || !preg_match('/^.+@.+\..+$/u', $_COOKIE['email_value'])) ? '' : strip_tags($_COOKIE['email_value']);
  $values['date'] = (empty($_COOKIE['date_value'])) ? "1900" : strip_tags($_COOKIE['date_value']);
  $values['sex'] = (empty($_COOKIE['sex_value'])) ? '' : strip_tags($_COOKIE['sex_value']);
  $values['limbs'] = (empty($_COOKIE['limbs_value'])) ? '' : strip_tags($_COOKIE['limbs_value']);
  $values['biography'] = (empty($_COOKIE['biography_value'])) ? '' : strip_tags($_COOKIE['biography_value']);
  $values['agree'] = (empty($_COOKIE['agree_value'])) ? '' : strip_tags($_COOKIE['agree_value']);

  $values['abilities'] = [];
  if (!empty($_COOKIE['abilities_value'])) {
    $abilities_cookies = json_decode(strip_tags($_COOKIE['abilities_value']));
    if(is_array($abilities_cookies)) {
      foreach($abilities_cookies as $ability) {
        if (!empty($abilities[$ability])) {
          $values['abilities'][$ability] = $ability;
        }
      }
    }
  }
  // TODO: аналогично все поля.

  // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
  // ранее в сессию записан факт успешного логина.


  if (empty(array_filter($errors)) && !empty($_COOKIE[session_name()]) && session_start()
     && !empty($_SESSION['login'])) {
    // TODO: загрузить данные пользователя из БД
    // и заполнить переменную $values,
    // предварительно санитизовав.
      printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
      $user = 'u20366';
      $pass = '5918475';
      $db = new PDO('mysql:host=localhost;dbname=u20366', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

      // Подготовленный запрос. Не именованные метки.

      try {
        $stmt = $db->prepare("SELECT * FROM application WHERE id = ? ");
        $stmt->execute(array($_SESSION['uid']));
      }
      catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
      } 

      $user_data = $stmt->fetchAll();

      $values['fio'] = !empty($user_data[0]['name']) ? strip_tags($user_data[0]['name']) : '';
      $values['email'] = !empty($user_data[0]['email']) ? strip_tags($user_data[0]['email']) : '';
      $values['date'] = !empty($user_data[0]['birth_year']) ? (int)$user_data[0]['birth_year'] : '';
      $values['sex'] = !empty($user_data[0]['sex']) ? strip_tags($user_data[0]['sex']) : '';
      $values['limbs'] = !empty($user_data[0]['limbs']) ? (int) $user_data[0]['limbs'] : '';
      $values['biography'] = !empty($user_data[0]['biography']) ? strip_tags($user_data[0]['biography']) : '';
      $values['agree'] = !empty($user_data[0]['agree']) ? strip_tags($user_data[0]['agree']) : '';
      $values['abilities'] = !empty($user_data[0]['ability']) ? explode(" ", strip_tags($user_data[0]['ability'])) : '';
      $ability = implode(' ', $values['abilities']);


    }  

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
  $errors = FALSE;
  if (empty($_POST['fio'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else if (!preg_match('/^[а-яА-Я ]+$/u', $_POST['fio'])) {
    setcookie('fio_error', '2', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['email'])) {
    // Выдаем куку на день с флажком об ошибке в поле email.
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else if (!preg_match('/^.+@.+\..+$/u', $_POST['email'])) {
    setcookie('email_error', '2', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['date'])) {
    // Выдаем куку на день с флажком об ошибке в поле date.
    setcookie('date_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('date_value', (int) $_POST['date'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['abilities'])) {
    // Выдаем куку на день с флажком об ошибке в поле abilities.
    setcookie('abilities_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    foreach($_POST['abilities'] as $ability) {
      if (empty($abilities[$ability])) {
        print('Нельзя');
        exit();
      }
    }
    setcookie('abilities_value', json_encode($_POST['abilities']), time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['sex'])) {
    // Выдаем куку на день с флажком об ошибке в поле abilities.
    setcookie('sex_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['limbs'])) {
    // Выдаем куку на день с флажком об ошибке в поле abilities.
    setcookie('limbs_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('limbs_value', (int) $_POST['limbs'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['biography'])) {
    // Выдаем куку на день с флажком об ошибке в поле biography.
    setcookie('biography_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['agree'])) {
    // Выдаем куку на день с флажком об ошибке в поле agree.
    setcookie('agree_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('agree_value', $_POST['agree'], time() + 30 * 24 * 60 * 60);
  }


  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('date_error', '', 100000);
    setcookie('sex_error', '', 100000);
    setcookie('limbs_error', '', 100000);
    setcookie('abilities_error', '', 100000);
    setcookie('biography_error', '', 100000);
    setcookie('agree_error', '', 100000);
    // TODO: тут необходимо удалить остальные Cookies.
  }

  // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
  if (!empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['login'])) {
    $ability = $_POST['abilities'];
    $ability = implode(' ', $_POST['abilities']);
    $user = 'u20366';
    $pass = '5918475';
    $db = new PDO('mysql:host=localhost;dbname=u20366', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

    //print_r($_SESSION['token']);
    //print_r($_POST['csrf_token']);
    //exit();
    if($_SESSION['token'] != $_POST['csrf_token']) {
        setcookie('token_error', 1, time() + 30 * 24 * 60 * 60);
        print_r("Что-то пошло не так! Повторите отправку формы");
        header('Location: index.php');
        exit();
      }

    try {
      $stmt = $db->prepare("UPDATE application SET name = ?, email = ?, birth_year = ?, sex = ?, limbs = ?, ability = ?, biography = ?, agree = ? WHERE id = ?");
      $stmt->execute(array($_POST['fio'], $_POST['email'], (int) $_POST['date'], $_POST['sex'], (int)$_POST['limbs'], $ability, $_POST['biography'], $_POST['agree'], $_SESSION['uid']));
    }
    catch(PDOException $e) {
      print ('Error : ' . $e->getMessage());
      exit();
    }
    // TODO: перезаписать данные в БД новыми данными,
    // кроме логина и пароля.
  }
  else {
    // Генерируем уникальный логин и пароль.
    // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
    $user = 'u20366';
    $pass = '5918475';
    $passw =  substr(md5(uniqid()),20);
    setcookie('passw', $passw, time() + 30 * 24 * 60 * 60);
    $db = new PDO('mysql:host=localhost;dbname=u20366', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    try {
      $stmt = $db->prepare("INSERT INTO application (name, email, sex, biography, birth_year, limbs, ability, agree, password) VALUES (:fio, :email, :sex, :biography, :date, :limbs, :ability, :agree, :password)");
      $fio = $_POST['fio'];
      $stmt->bindParam(':fio', $fio, PDO::PARAM_STR);
      $email = $_POST['email'];
      $stmt->bindParam(':email', $email, PDO::PARAM_STR);
      $sex = $_POST['sex'];
      $stmt->bindParam(':sex', $sex, PDO::PARAM_STR);
      $biography = $_POST['biography'];
      $stmt->bindParam(':biography', $biography, PDO::PARAM_STR);
      $date = $_POST['date'];
      $stmt->bindParam(':date', $date, PDO::PARAM_STR);
      $limbs = $_POST['limbs'];
      $stmt->bindParam(':limbs', $limbs, PDO::PARAM_INT);
      $ability = $_POST['abilities'];
      $ability = implode(' ', $_POST['abilities']);
      $stmt->bindParam(':ability', $ability, PDO::PARAM_STR);
      $agree = $_POST['agree'];
      $stmt->bindParam(':agree', $agree, PDO::PARAM_STR);
      $stmt->bindParam(':password', $passw, PDO::PARAM_STR);
      $stmt->execute();
    }
    catch(PDOException $e) {
      print ('Error : ' . $e->getMessage());
      exit();
    }
    $current_id=$db->lastInsertId();
    $login = 'user'.$db->lastInsertId();
    setcookie('login', $login, time() + 30 * 24 * 60 * 60);
    try {
      $params=array($login,$current_id);
      $stmt = $db->prepare("UPDATE application SET login = ? WHERE id = ?");
      $stmt -> execute($params);
    }
    catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
    }
  // Сохраняем куку с признаком успешного сохранения.
  }
  setcookie('save', '1');
  // Делаем перенаправление.
  header('Location: ./');
}
