<?php

/**
 * Задача 6. Реализовать вход администратора с использованием
 * HTTP-авторизации для просмотра и удаления результатов.
 **/

// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.


//*********login: admin1
//*********password: 123

$user = 'u20366';
$pass = '5918475';
$db = new PDO('mysql:host=localhost;dbname=u20366', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

try {
  $stmt = $db->prepare("SELECT * FROM admins");
  $stmt->execute();
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
} 

$admin_data = $stmt->fetchAll();

foreach($admin_data as $row) {
    $a_login = strip_tags($row['login']);
    $a_pass = strip_tags($row['pass']);
}


if (empty($_SERVER['PHP_AUTH_USER']) ||
    empty($_SERVER['PHP_AUTH_PW']) ||
    $_SERVER['PHP_AUTH_USER'] != $a_login ||
    substr(md5($_SERVER['PHP_AUTH_PW']), 19) != $a_pass) {
  header('HTTP/1.1 401 Unanthorized');
  header('WWW-Authenticate: Basic realm="My site"');
  print('<h1>401 Требуется авторизация</h1>');
  exit();
}

print('Вы успешно авторизовались и видите защищенные паролем данные.');

// *********
// Здесь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
// Реализовать просмотр и удаление всех данных.
// *********

// Подготовленный запрос. Не именованные метки.
try {
  $stmt = $db->prepare("SELECT * FROM application");
  $stmt->execute();
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
} 

$user_data = $stmt->fetchAll();

  $values= [];
  foreach($user_data as $row) { 
    $name = strip_tags($row['name']); //выбрасываем теги
    $email = htmlspecialchars($row['email']); //заменяем опасные символы на мнемоники
    $login = strip_tags($row['login']); //выбрасываем теги
    $password = strip_tags($row['password']); //выбрасываем теги
    $year = (int) $row['birth_year']; //приводим полученное значение к целочисленному типу
    $limbs = (int) $row['limbs']; //приводим полученное значение к целочисленному типу
    if ($limbs == 6) {
      $limbs = "Шесть";
    }
    if ($limbs == 2) {
      $limbs = "Две";
    }

    $sex = $row['sex'] == "M" ? 'Мужской' : 'Женский';
    
    $abilities_eng = explode(" ", strip_tags($row['ability']));
    $abilities = [];
    foreach($abilities_eng as $key => $value) {
      if($value == "Immortality") {
        $abilities[$key] = "Бессмертие";
      }
      if($value == "Througn_the_walls") {
        $abilities[$key] = "Леха";
      }
      if($value == "Levitation") {
        $abilities[$key] = "Левитация";
      }
    }

    $ability = implode(', ', $abilities);

    $biography = strip_tags($row['biography']);
    $agree = strip_tags($row['agree']);
    //print_r(array($abilities));
    //exit();
    $values[$row['id']] = [
      $name,
      $email,
      $login,
      $password,
      $year,
      $sex,
      $limbs,
      $biography,
      $agree,
      $ability
    ];
  }
  //print_r($abilities);
  //exit();
  //print_r($values);
  //exit();
  $columns = ['Имя', 'E-mail', 'Логин', 'Пароль', 'Год', 'Пол', 'Конечности', 'Биография', 'Согласие', 'Способности'];
  include('table.php');


  # Если кнопка нажата
  if(isset($_POST['del']))
    {
      $val = $_POST['del'];
      $user = 'u20366';
      $pass = '5918475';
      $db = new PDO('mysql:host=localhost;dbname=u20366', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

      try {
        $stmt = $db->prepare("DELETE FROM application WHERE id = $val");
        $stmt->execute();
      }
      catch(PDOException $e) {
        print ('Error : ' . $e->getMessage());
        exit();
      }
      header('Location: admin.php');
    }
?>

  












