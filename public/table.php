<!DOCTYPE html>
<html>
    <head> 
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"              integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
        <title>Table</title>
        <link rel='stylesheet' href='style.css'/>
    </head>

    <body>

        <table class="table">
            <thead>
                <tr>
                    <?php
                    foreach($columns as $column) {
                        print('<th scope="col">'.$column.'</th>');
                    }
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach($values as $key => $value) {
                    //print_r($key);
                    print('<tr>');
                    foreach($value as $person) {
                        print('<td>'.$person.'</td>');
                    }
                    print('<td>
                    <form action="admin.php" method="POST">
                    <button class="btn" value="'.$key.'" name="del" type="submit">Удалить</button>
                    </form>
                    </td>');
                    print('</tr>');
                }
                ?>
            </tbody>
        </table>

    </body>
